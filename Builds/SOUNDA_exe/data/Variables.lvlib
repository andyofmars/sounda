﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="19008000">
	<Property Name="NI.Lib.Icon" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5X&lt;A91M&lt;/W-,&lt;'&amp;&lt;9+K1,7Q,&lt;)%N&lt;!NMA3X)DW?-RJ(JQ"I\%%Z,(@`BA#==ZB3RN;]28_,V7@P_W`:R`&gt;HV*SU_WE@\N_XF[3:^^TX\+2YP)D7K6;G-RV3P)R`ZS%=_]J'XP/5N&lt;XH,7V\SEJ?]Z#5P?=J4HP+5JTTFWS%0?=B$DD1G(R/.1==!IT.+D)`B':\B'2Z@9XC':XC':XBUC?%:HO%:HO&amp;R7QT0]!T0]!S0I4&lt;*&lt;)?=:XA-(]X40-X40-VDSGC?"GC4N9(&lt;)"D2,L;4ZGG?ZH%;T&gt;-]T&gt;-]T?.S.%`T.%`T.)^&lt;NF8J4@-YZ$S'C?)JHO)JHO)R&gt;"20]220]230[;*YCK=ASI2F=)1I.Z5/Z5PR&amp;)^@54T&amp;5TT&amp;5TQO&lt;5_INJ6Z;"[(H#&gt;ZEC&gt;ZEC&gt;Z$"(*ETT*ETT*9^B)HO2*HO2*(F.&amp;]C20]C2)GN4UE1:,.[:/+5A?0^NOS?UJ^3&lt;*\9B9GT@7JISVW7*NIFC&lt;)^:$D`5Q9TWE7)M@;V&amp;D,6;M29DVR]6#R],%GC47T9_/=@&gt;Z5V&gt;V57&gt;V5E&gt;V5(OV?^T[FTP?\`?YX7ZRP6\D=LH%_8S/U_E5R_-R$I&gt;$\0@\W/VW&lt;[_"&lt;Y[X&amp;],0^^+,]T_J&gt;`J@_B_]'_.T`$KO.@I"O[^NF!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">419463168</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Item Name="Channel Data" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typedefName1" Type="Str">Channel.lvlib:Plotter.lvlib:Channel Data.ctl</Property>
		<Property Name="typedefName2" Type="Str">Channel.lvlib:Queue Data.ctl</Property>
		<Property Name="typedefPath1" Type="PathRel">../../SOUNDA.exe/panda_logger/Project/Channel/Plotter/_TypeDef/Channel Data.ctl</Property>
		<Property Name="typedefPath2" Type="PathRel">../../../../Project/panda_logger/Project/Channel/_TypeDef/Queue Data.ctl</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$:V1!!!"E!A!!!!!!,!""!-0````](1WBB&lt;GZF&lt;!!11$$`````"V.U=G6B&lt;8-!&amp;%"!!!(`````!!%(5X2S:7&amp;N=Q!'!&amp;1!"A!91%!!!@````]!!QJ5;7VF=X2B&lt;8"T!!!&amp;!!I!!"*!1!!"`````Q!&amp;"%2B&gt;'%!!"J!5!!#!!1!"AZP&gt;82Q&gt;81A9WRV=X2F=A!!'%"!!!(`````!!=,5X2S:7&amp;N)%2B&gt;'%!&amp;%"1!!-!!!!#!!A(1WRV=X2F=A!31%!!!@````]!#16"=H*B?1!"!!I!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Globals" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typedefName1" Type="Str">Channel.lvlib:Syslog Severity Codes.ctl</Property>
		<Property Name="typedefName10" Type="Str">Globals_Logging.ctl</Property>
		<Property Name="typedefName11" Type="Str">Globals_State.ctl</Property>
		<Property Name="typedefName12" Type="Str">Globals_System.ctl</Property>
		<Property Name="typedefName2" Type="Str">Channel.lvlib:Philips.lvlib:Philips_Monitor_Mode.ctl</Property>
		<Property Name="typedefName3" Type="Str">Channel.lvlib:Syslog Severity Codes.ctl</Property>
		<Property Name="typedefName4" Type="Str">Globals.ctl</Property>
		<Property Name="typedefName5" Type="Str">Globals_Config.ctl</Property>
		<Property Name="typedefName6" Type="Str">Globals_Demographics.ctl</Property>
		<Property Name="typedefName7" Type="Str">Globals_Indicator.ctl</Property>
		<Property Name="typedefName8" Type="Str">Globals_IO.ctl</Property>
		<Property Name="typedefName9" Type="Str">Globals_IRDriver.ctl</Property>
		<Property Name="typedefPath1" Type="PathRel">../../SOUNDA.exe/panda_logger/Project/Channel/_TypeDef/Syslog Severity Codes.ctl</Property>
		<Property Name="typedefPath10" Type="PathRel">../../../../Project/panda_logger/Project/Utility/Variables/Globals_Logging.ctl</Property>
		<Property Name="typedefPath11" Type="PathRel">../../../../Project/panda_logger/Project/Utility/Variables/Globals_State.ctl</Property>
		<Property Name="typedefPath12" Type="PathRel">../../../../Project/panda_logger/Project/Utility/Variables/Globals_System.ctl</Property>
		<Property Name="typedefPath2" Type="PathRel">../../../../Project/panda_logger/Project/Channel/Philips/_TypeDef/Philips_Monitor_Mode.ctl</Property>
		<Property Name="typedefPath3" Type="PathRel">../../../../Project/panda_logger/Project/Channel/_TypeDef/Syslog Severity Codes.ctl</Property>
		<Property Name="typedefPath4" Type="PathRel">../../../../Project/panda_logger/Project/Utility/Variables/Globals.ctl</Property>
		<Property Name="typedefPath5" Type="PathRel">../../../../Project/panda_logger/Project/Utility/Variables/Globals_Config.ctl</Property>
		<Property Name="typedefPath6" Type="PathRel">../../../../Project/panda_logger/Project/Utility/Variables/Globals_Demographics.ctl</Property>
		<Property Name="typedefPath7" Type="PathRel">../../../../Project/panda_logger/Project/Utility/Variables/Globals_Indicator.ctl</Property>
		<Property Name="typedefPath8" Type="PathRel">../../../../Project/panda_logger/Project/Utility/Variables/Globals_IO.ctl</Property>
		<Property Name="typedefPath9" Type="PathRel">../../../../Project/panda_logger/Project/Utility/Variables/Globals_IRDriver.ctl</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!R03QQ!!"E!A!!!!!"U!"2!-0````],5G6N&lt;X2F)%F18T%!&amp;%!Q`````QN3:7VP&gt;'5A36"@-A!?1$$`````&amp;%:J&lt;'6O97VF)%:P=GVB&gt;#"4='6D!!!D1!)!(%2F:G&amp;V&lt;(1A5X2S:7&amp;N)%*V:G:F=C"-:7ZH&gt;'A!!"*!-P````]*9G&amp;T:3"Q982I!%6!&amp;A!)"76N:8*H"7&amp;M:8*U"'.S;81$:8*S"(&gt;B=GY'&lt;G^U;7.F"'FO:G]&amp;:'6C&gt;7=!%%VJ&lt;C"-:8:F&lt;#!I:'FT=#E!!%.!&amp;A!)"76N:8*H"7&amp;M:8*U"'.S;81$:8*S"(&gt;B=GY'&lt;G^U;7.F"'FO:G]&amp;:'6C&gt;7=!$UVJ&lt;C"-:8:F&lt;#!I&lt;'^H+1!=1&amp;!!"Q!!!!%!!A!$!!1!"1!'"E.P&lt;G:J:Q!!$E!Q`````Q21&lt;X*U!!!51$$`````#V*F=W6S&gt;G6E)%*Z!!R!6!!'"&amp;2J&lt;75!!"J!5!!$!!A!#1!+$6*F=W6S&gt;G6E)&amp;"P=H1!)%"!!!(`````!!M35G6T:8*W:71A1U^.)("P=H2T!!!-1&amp;!!!1!-!UEP4Q!11$$`````"EFO:G&amp;O&gt;!!!%U!'!!VQ982@:'VH8X.U982F!!^!"A!)='&amp;U8X2Z='5!!"6!"A!/='&amp;U8X"B9W6E8WVP:'5!!"2!-0````]+:WFW:7Z@&lt;G&amp;N:1!!&amp;%!Q`````QNG97VJ&lt;(F@&lt;G&amp;N:1!11$$`````"H"B&gt;&amp;^J:!!!$5!'!!&gt;Q982@=W6Y!!V!"1!(9W6O&gt;(6S?1!,1!5!"(FF98)!!!N!"1!&amp;&lt;7^O&gt;'A!#5!&amp;!!.E98E!#U!&amp;!!2I&lt;X6S!!!.1!5!"GVJ&lt;H6U:1!!$5!&amp;!!:T:7.P&lt;G1!!!^!"1!):H*B9X2J&lt;WY!!"Z!5!!)!"9!&amp;Q!9!"E!'A!&lt;!"Q!(1&gt;Q982@:'^C!!N!"A!&amp;&gt;7ZJ&gt;(-!%E!Q`````QFV&lt;GFU=V^T&gt;()!#U!+!!6W97RV:1!91&amp;!!!Q!@!#!!)1JQ982@;'6J:WBU!!!91&amp;!!!Q!@!#!!)1JQ982@&gt;W6J:WBU!!!51&amp;!!!Q!@!#!!)1&gt;Q982@97&gt;F!$*!5!!,!!]!%!!2!")!%Q!5!"5!(A!C!#-!*"21;'FM;8"T)%2F&lt;7^H=G&amp;Q;'FD=Q!!'%"1!!)!$A!F$%2F&lt;7^H=G&amp;Q;'FD=Q!!'E!Q`````R"4&lt;W:U&gt;W&amp;S:3"7:8*T;7^O!!!91$$`````$F.D;'6N93"W:8*T;7^O!!!71$$`````$&amp;*V&lt;H2J&lt;75A47^E:1!!%E!Q`````QB)&lt;X.U&lt;G&amp;N:1!!%%!Q`````Q:T?8.@;71!!":!-0````]-&lt;7&amp;O&gt;7:B9X2V=G6S!!!71$$`````$'VP:'6M8WZV&lt;7*F=A!!&amp;%"1!!)!,!!N#8.Z=V^N&lt;W2F&lt;!!21!=!#WZP&lt;6^W:8*T;7^O!"N!"Q!6&gt;'6Y&gt;&amp;^D982B&lt;'^H8X*F&gt;GFT;7^O!!^!"A!)&lt;'&amp;O:X6B:W5!!!V!"A!':G^S&lt;7&amp;U!!!71&amp;!!!Q!Q!$%!-AFT?8.@&lt;'^D97Q!%U!'!!RT?8.@98.T&lt;W.@;71!!"&amp;!"A!+&lt;72T8X.U982V=Q!!%E!Q`````QFC:72@&lt;'&amp;C:7Q!$5!'!!&gt;P=&amp;^N&lt;W2F!!^!"A!)98"Q8W&amp;S:7%!!!^!"A!*=X"F9V^U?8"F!".!"A!-9W^N='^O:7ZU8WFE!!!31$$`````#8"S&lt;W2@=X"F9Q!11&amp;!!!Q!Z!$I!/Q.W97Q!&amp;E"!!!(`````!$Q)=XFT8X.Q:7-!!#J!5!!+!#M!,A!P!$-!.!!V!$9!.Q!Y!$U/5'BJ&lt;'FQ=S"4?8.U:7U!!!R!-0````]$1G6E!)V!&amp;A!)#V6O=X"F9WFG;76E&amp;66O=X"F9WFG;76E)#UA5X2B&lt;G2C?12%:7VP$E2F&lt;7]A,3"4&gt;'&amp;O:'*Z#EVP&lt;GFU&lt;X*J&lt;G=547^O;82P=GFO:S!N)&amp;.U97ZE9HE(5W6S&gt;GFD:2&amp;4:8*W;7.F)#UA5X2B&lt;G2C?1!55'BJ&lt;'FQ=S".&lt;WZJ&gt;'^S)%VP:'5!!"R!5!!(!#=!+!!J!#I!0A!`!%!'5XFT&gt;'6N!!!=1#%827ZB9GRF)%RP:W&gt;J&lt;G=A+%&gt;M&lt;W*B&lt;#E!(%!B&amp;U6O97*M:3"-&lt;W&gt;H;7ZH)%^W:8*S;72F!":!)2&amp;&amp;&lt;G&amp;C&lt;'5A5'RP&gt;(2J&lt;G=A-A!G1%!!!@````]!2"B&amp;&lt;G&amp;C&lt;'5A4'^H:WFO:S!I1WBB&lt;GZF&lt;#E!!!1!)1!11%!!!@````]!2A."2%-!%E"!!!(`````!%9&amp;5(6M=W5!'%"!!!(`````!%9+5(*P?(FU=G&amp;D;Q!!&amp;%"!!!(`````!%9(5'BJ&lt;'FQ=Q!11%!!!@````]!2A.$15U!&amp;%"!!!(`````!%9'5XFT&lt;'^H!!!71%!!!@````]!2AF*5C"%=GFW:8)!,%"1!!=!2Q")!%E!3A",!%Q!42&gt;&amp;&lt;G&amp;C&lt;'5A4'^H:WFO:S!I=X2S:7&amp;N+1!51$,`````#E:J&lt;'6T)&amp;"B&gt;'A!!"2!-P````],37VB:W6T)&amp;"B&gt;'A!&amp;%!S`````QJ4&gt;(6E?3"1982I!!!=1&amp;!!"Q"#!%-!21"/!%]!5!"2"URP:W&gt;J&lt;G=!,U!7!!1&amp;26*34V)(6U&amp;34EF/2Q*03QB%36."1ER&amp;2!!-5XFT&gt;'6N)&amp;.U982F!!!P1"9!"!6&amp;5F*05A&gt;816*/35Z(!E^,#%2*5U&amp;#4%6%!!V/:82X&lt;X*L)&amp;.U982F!"F!!A!3:'6W;7.F8W&amp;M:8*U8X.U982F!!!=1&amp;!!!1"6%V"I;7RJ=(-A17RF=H1A5X2B&gt;'5!'U!+!"2'=G6F)&amp;.Q97.F)#AF)&amp;2P&gt;'&amp;M+1!!&amp;5!+!!^'=G6F)&amp;.Q97.F)#B(1CE!,U!7!!1&amp;26*34V)(6U&amp;34EF/2Q*03QB%36."1ER&amp;2!!.4'^H:WFO:S"4&gt;'&amp;U:1!P1"9!"!6&amp;5F*05A&gt;816*/35Z(!E^,#%2*5U&amp;#4%6%!!V(=G&amp;T:7*Z)&amp;.U982F!#V!&amp;A!%"5635E^3"V&gt;"5EZ*4E=#4UM)2%F415*-251!#E:J4T)A5X2B&gt;'5!!#V!&amp;A!%"5635E^3"V&gt;"5EZ*4E=#4UM)2%F415*-251!#V"S&lt;XBZ)&amp;.U982F!#^!&amp;A!%"5635E^3"V&gt;"5EZ*4E=#4UM)2%F415*-251!$6"I;7RJ=(-A5X2B&gt;'5!+U!7!!1&amp;26*34V)(6U&amp;34EF/2Q*03QB%36."1ER&amp;2!!*1W&amp;N)&amp;.U982F!"6!"!!06W^S;WFO:S"4:81A+%)J!#2!5!!-!&amp;-!6!"7!&amp;=!7!":!&amp;I!7Q"=!&amp;U!8A"@"6.U982F!!R!)1&gt;$15V@5&amp;&gt;3!!V!"1!(4%6%8V"841!21!5!#UR06U638UR*45F5!"&amp;!"1!,66"126*@4%F.361!(E"1!!1!91"C!'-!:!Z*5C"%=GFW:8)A2%&amp;511!!'U!7!!)&amp;26*34V)#4UM!!!F%26:@5V2"6%5!%%!Q`````Q&gt;726*435^/!"&amp;!"1!,4%F(3&amp;2@4%6725Q!*%"1!!=!:A"H!'%!9A"D!'1!;!^*5C"%=GFW:8)A5V2"6%5!$%!B"U&amp;V&gt;']A36)!&amp;E"1!!-!:1"J!'I*36)A2(*J&gt;G6S!!V!!A!(4%6%8V"841!31%!!!@````]!&lt;!646%&amp;521!91&amp;!!!1"N$EFO:'FD982P=C"%162"!!!21!)!#UR06U638UR*45F5!"2!1!!"`````Q"P"U*@5V2"6%5!(E"1!!1!:A"H!'U!=!^*&lt;G2J9W&amp;U&lt;X)A5V2"6%5!&amp;%"1!!)!&lt;A"R#5FO:'FD982P=A!?1&amp;!!#!!(!!U!*A""!&amp;)!9!"L!()(2WRP9G&amp;M=Q!"!(-!!!!!!!!!!!!!!!!!!&amp;"53$!!!!!%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"16%AQ!!!!"!!!!!"16%AQ!!!!"!!!!!"16%AQ!!!!"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Image" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typedefName1" Type="Str">Image Data.ctl</Property>
		<Property Name="typedefPath1" Type="PathRel">../../../../Project/panda_logger/Project/Utility/Variables/Image Data.ctl</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!^/1!!!"E!A!!!!!!#!!5!"Q!!(%"!!!,``````````Q!!#EFN97&gt;F)%2B&gt;'%!!!%!!1!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Indicated States" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typedefName1" Type="Str">Indicated States.ctl</Property>
		<Property Name="typedefPath1" Type="PathRel">../../../../Project/panda_logger/Project/Utility/Variables/Indicated States.ctl</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"N;1!!!"E!A!!!!!!%!"2!-0````]+5X2B&gt;'5A4G&amp;N:1!!%5!(!!N4&gt;'&amp;U:3"797RV:1!31&amp;!!!A!!!!%(1WRV=X2F=A!?1%!!!@````]!!B"*&lt;G2J9W&amp;U&lt;X)A5X2B&gt;'6T!!!"!!-!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="States" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typedefName1" Type="Str">Channel.lvlib:Execution State.ctl</Property>
		<Property Name="typedefName2" Type="Str">Channel.lvlib:Data State.ctl</Property>
		<Property Name="typedefName3" Type="Str">Channel.lvlib:Execution State.ctl</Property>
		<Property Name="typedefName4" Type="Str">States.ctl</Property>
		<Property Name="typedefPath1" Type="PathRel">../../SOUNDA.exe/panda_logger/Project/Channel/_TypeDef/Execution State.ctl</Property>
		<Property Name="typedefPath2" Type="PathRel">../../../../Project/panda_logger/Project/Channel/_TypeDef/Data State.ctl</Property>
		<Property Name="typedefPath3" Type="PathRel">../../../../Project/panda_logger/Project/Channel/_TypeDef/Execution State.ctl</Property>
		<Property Name="typedefPath4" Type="PathRel">../../../../Project/panda_logger/Project/Utility/Variables/States.ctl</Property>
		<Property Name="typeDesc" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;;6A%!!"E!A!!!!!!+!":!-0````]-1WBB&lt;GZF&lt;#"/97VF!!"(1"9!#!:$=G6B&gt;'5+37ZJ&gt;'FB&lt;'F[:1&gt;"9X&amp;V;8*F!URP:Q6$&lt;'^T:1&gt;%;8.D98*E"F.U982V=Q6$;'6D;Q!!"6.U97&gt;F!#&gt;!&amp;A!%!E^,"5635E^3#%2*5U&amp;#4%6%"V6/3UZ06UY!"6.U982F!$6!&amp;A!%"UF/6E&amp;-351&amp;6E&amp;-351$4C^"$UF/6E&amp;-351A,3"&amp;5F*05A!+2'&amp;U93"4&gt;'&amp;U:1!!(E"!!!(`````!!-25X2S:7&amp;N)%2B&gt;'%A5X2B&gt;'5!'%!Q`````Q^-98.U)&amp;.Z=WRP:S".=W=!'5!$!"-D)'6M:7VF&lt;H2T)'FO)(&amp;V:86F!"*!6!!'#F2J&lt;75A5X2B&lt;8!!!"1!5!!(!!!!!1!#!!1!"1!'!!=!&amp;%"!!!(`````!!A'5X2B&gt;'6T!!!"!!E!!!!!!!!!!!!!!!!</Property>
	</Item>
</Library>
